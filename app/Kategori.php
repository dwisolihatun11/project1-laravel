<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "m_kategori";
    protected $fillable = ["kategori"];

    public function berita(){
        return $this->hasMany('App\Berita');
    }
}
