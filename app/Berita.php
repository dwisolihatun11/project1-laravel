<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = "t_berita";
    protected $fillable = ["title", "content", "kategori_id", "user_id", "photo", "created_at"];

    public function kategori(){
        return $this->belongsTo('App\Kategori');
    }
    public function komentar(){
        return $this->hasMany('App\Komentar');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
