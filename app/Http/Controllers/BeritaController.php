<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Berita;
use App\Kategori;
use App\Komentar;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use File;

class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
        // $this->middleware('log')->only('index');
        // $this->middleware('subscribed')->except('store');
    }

    public function create(){
        $kategori = Kategori::pluck('kategori', 'id');
        return view('berita.create', compact('kategori'));
    }

    public function store(Request $request){
        $kategori_id = (int)$request->kategori_id;
        $user_id = Auth::id();
        $file = $request->file('photo');
        $tujuan_upload = 'data_file';

        $validated = $request->validate([
            'title' => 'required',
            'content' => 'required',
            'photo' => 'required',
        ]);

        if($file->move($tujuan_upload,$file->getClientOriginalName())){
            Berita::create([
                'title' => $request->title,
                'photo' => $file->getClientOriginalName(),
                'content' => $request->content,
                'kategori_id' => $kategori_id,
                'user_id' => $user_id,
            ]);
        }
        Alert::success('Berhasil', 'Tambah Data Berhasil');
        return redirect('/berita');
    }

    public function index(){
        $berita = Berita::all();
        $kategori = Kategori::all();
        $komentar = Komentar::all(); // nambahin ini
        return view('berita.index', compact('berita', 'kategori', 'komentar'));
    }

    public function show($id){
        $berita = Berita::find($id);
        // $komentar = Komentar::all();
        // $komen = Berita::find($komentar->berita->id); // nambahin ini
        // dd($komentar);
        return view('berita.show', compact('berita'));
    }

    public function edit($id){
        $berita = Berita::find($id);
        $kategori = Kategori::find($berita->kategori_id);
        // dd($kategori);
        $kategoris = Kategori::pluck('kategori', 'id');
        return view('berita.edit', compact('berita','kategoris','kategori'));
    }

    public function update($id, Request $request){
        $user_id = Auth::id();
        $kategori_id = (int)$request->kategori_id;
        $file = $request->file('photo');
        $tujuan_upload = 'data_file';

        $validated = $request->validate([
            'title' => 'required',
            'content' => 'required',
            'photo' => 'required'
        ]);

        $get_data = Berita::find($id);
        $destinationPath = 'data_file';
        File::delete($destinationPath.'/'.$get_data->photo.'');

        if($file->move($tujuan_upload,$file->getClientOriginalName())){
            $berita = Berita::find($id);
            $berita->title = $request->title;
            $berita->content = $request->content;
            $berita->photo = $file->getClientOriginalName();
            $berita->user_id = $user_id;
            $berita->kategori_id = $kategori_id;
            $berita->update();
        }


        Alert::success('Update', 'Tambah Data Berhasil');
        return redirect('/berita');
    }

    public function destroy($id){
        DB::table('t_comment')->where('berita_id', $id)->delete();
        $berita = Berita::find($id);
        $berita->delete();

        Alert::warning('Hapus', 'Hapus Data Berhasil');
        return redirect('/berita');
    }
}
