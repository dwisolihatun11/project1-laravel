## Final Project

## Kelompok 17

Anggota Kelompok

<ol>
<li>Rochman Ari Wibowo</li>
<li>Dwi Solihatun</li>
<li>Gavin Liffera</li>
</ol>

## Tema Project

Portal Berita

## ERD

<img src="public/template/dist/img/ERD.png" width="400">

## Link Video

<ul>
<li>Link Video Demo : https://drive.google.com/file/d/1p-KhndfoBZpDBJA7Dq3vZJZjYvUTBr5W/view?usp=sharing </li>
<li>Link Deploy : https://project-laravel-kel17.herokuapp.com/public </li>
<ul>
