<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'BeritaController@index');

//REGISTER
Route::get('/register', 'RegisterController@index');

//LOGIN
Route::get('/login', 'LoginController@index');

Route::group(['middleware' => ['auth']], function () {
    //BERITA
    Route::get('/berita/create', 'BeritaController@create');
    Route::post('/berita', 'BeritaController@store');
    Route::get('/berita', 'BeritaController@index');
    Route::get('/berita/{berita_id}', 'BeritaController@show');
    Route::get('/berita/{berita_id}/edit', 'BeritaController@edit');
    Route::put('/berita/{berita_id}', 'BeritaController@update');
    Route::delete('/berita/{berita_id}', 'BeritaController@destroy');

    Route::resource('profile', 'ProfileController')->only([
        'index','update'
    ]);

    Route::resource('komentar', 'KomentarController')->only([
        'index','store'
    ]);

    // Route::post('/like', 'LikeController@store');
    // Route::get('/like', 'BeritaController@index');

    // Route::resource('like', 'LikeController')->only([
    //     'store', 'index'
    // ]);

    //KATEGORI
    Route::get('/kategori/create', 'KategoriController@create');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori', 'KategoriController@index');
    Route::get('/kategori/{kategori_id}', 'KategoriController@show');
    Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
    Route::put('/kategori/{kategori_id}', 'KategoriController@update');
    Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

    //KOMENTAR
    Route::get('/komentar/create/{berita_id}', 'KomentarController@create');
    Route::post('/komentar', 'KomentarController@store');
    Route::get('/komentar', 'KomentarController@index');
    Route::get('/komentar/{komentar_id}', 'KomentarController@show');
    Route::get('/komentar/{komentar_id}/edit', 'KomentarController@edit');
    Route::put('/komentar/{komentar_id}', 'KomentarController@update');
    Route::delete('/komentar/{komentar_id}', 'KomentarController@destroy');

    //PROFILE
    Route::get('/profile/create', 'ProfileController@create');
    Route::post('/profile', 'ProfileController@store');
    Route::get('/profile', 'ProfileController@index');
    Route::get('/profile/{profile_id}', 'ProfileController@show');
    Route::get('/profile/{profile_id}/edit', 'ProfileController@edit');
    Route::put('/profile/{profile_id}', 'ProfileController@update');
    Route::delete('/profile/profile_id}', 'ProfileController@destroy');
});

Route::resource('berita', 'BeritaController');
Auth::routes();


