@extends('layout.master')
@section('judul')
    Halaman Edit kategori
@endsection

    @section('content')
    <form action="/kategori/{{$kategori->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Judul</label>
          <input type="text" name="kategori" value="{{$kategori->kategori}}" class="form-control">
        </div>
        @error('kategori')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    @endsection
