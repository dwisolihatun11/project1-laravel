@extends('layout.master')
@section('judul')
    Halaman Detail kategori {{$kategori->kategori}}
@endsection

    @section('content')
   
        <ol>
            @forelse ($kategori->berita as $item)
                <li>
                    <b>{{$item->title}}</b> 
                    <br>
                    <b>Dibuat pada -</b> {{$item->created_at}}
                    <br>
                    <b>Oleh -</b> {{$item->user->name}}
                </li>
                @empty
                <p>Belum Ada Berita di Kategori Ini</p>
            @endforelse
        </ol>



    @endsection
