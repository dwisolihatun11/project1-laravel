@extends('layout.master')
@section('judul')
    Halaman List kategori
@endsection

    @section('content')
    <a class="btn btn-success btn-sm mb-3" href="/kategori/create">Tambah Data</a>
    <table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Kategori</th>
        <th scope="col">Berita</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse($kategori as $key =>$item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->kategori}}</td>
            <td>
                <ol>@foreach($item->berita as $value)
                    <li>{{$value->title}}</li>
                    @endforeach
                </ol>
            </td>
            <td>
                <form action="/kategori/{{$item->id}}" method="post">
                    <a class="btn btn-info btn-sm" href="/kategori/{{$item->id}}">Detail</a>
                    <a class="btn btn-warning btn-sm" href="/kategori/{{$item->id}}/edit">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
      @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
      @endforelse
    </tbody>
  </table>
  @endsection
