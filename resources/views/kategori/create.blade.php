@extends('layout.master')
@section('judul')
    Halaman Tambah Kategori
@endsection

    @section('content')
    <form action="/kategori" method="post">
        @csrf
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Kategori</label>
          <input type="text" name="kategori" class="form-control">
        </div>
        @error('kategori')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    @endsection
