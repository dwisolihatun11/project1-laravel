@extends('layout.master')
@section('judul')
Berita Terkini Versi Portal<b>Berita</b>
@endsection

@section('content')

@auth
<a href="/berita/create" class="btn btn-default btn-sm mb-3"><i class="fas fa-plus-circle"></i> Tambah Berita</a>
@endauth
      <div class="row">
        @forelse($berita as $key =>$item)
          <div class="col-md-4">
            <!-- Box Comment -->
            <div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <h4>
                    <b>{{$item->title}}</b>
                  </h4>
                  <span><b>Dibuat pada -</b> {{$item->created_at}}</sapan> <br>
                  <span><b>Kategori Berita -</b> {{$item->kategori->kategori}}</span>

                  @auth
                  <form action="/berita/{{$item->id}}" method="post">
                      <a href="/berita/{{$item->id}}/edit" class="btn btn-default btn-sm"><i class="fas fa-edit"></i> Edit Berita</a>
                     @csrf
                      @method('delete')
                      <input type="submit" class="btn btn-default btn-sm" value="Hapus Berita">
                    </form>
                  @endauth
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <img class="img-fluid" src="{{ asset('data_file/' . $item->photo) }}" alt="Photo">
                <br>
                <p>{{strip_tags($item->content)}}</p>


                <a href="/berita/{{$item->id}}" class="btn btn-default btn-sm"><i class="fas fa-comments"></i> Comments</a>

                <!-- <span class="float-right text-muted">127 likes</span> -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          @empty
          @endforelse
      </div>

    <!-- @auth
    <a class="btn btn-success btn-sm mb-3" href="/berita/create">Tambah Data</a>
    <table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Title</th>
        <th scope="col">Kategori</th>
        <th scope="col">Content</th>
        <th scope="col">Photo</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse($berita as $key =>$item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->title}}</td>
            <td>{{$item->kategori->kategori}}</td>
            <td>{{$item->content}}</td>
            <td><img src="{{ asset('data_file/' . $item->photo) }}" class="css-class" style="width:100px;height:75px;"></td>
            <td>
                <div class="row">
                  <form action="/berita/{{$item->id}}" method="post">
                      <a class="btn btn-info btn-sm" href="/berita/{{$item->id}}">Detail</a>
                      <a class="btn btn-warning btn-sm" href="/berita/{{$item->id}}/edit">Edit</a>
                      @csrf
                      @method('delete')
                      <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                  </form>
                </div>

            </td>
        </tr>
      @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
      @endforelse
    </tbody>
  </table>

  @endauth -->
  @endsection
