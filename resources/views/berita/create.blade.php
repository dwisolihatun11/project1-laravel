@extends('layout.master')
@section('judul')
    Halaman Tambah Berita
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/53a1xndfmu73tldyhd86jij8c39zsbmsdyvvpypnxatq25tb/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
   tinymce.init({
      selector: 'textarea',
      plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
    ],
        toolbar: 'undo redo | formatselect | ' +
    'bold italic backcolor | alignleft aligncenter ' +
    'alignright alignjustify | bullist numlist outdent indent | ' +
    'removeformat | help',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        });
  </script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
      // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
    </script>
@endpush
    @section('content')
    <form action="/berita" method="post"  enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Judul</label>
          <input type="text" name="title" class="form-control">
        </div>
        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Kategori</label><br>
            <select name="kategori_id" id="kategori_id" class="js-example-basic-single" style="width:100%;">
                <option value="">== Pilih Kategori ==</option>
                @foreach ($kategori as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                @endforeach
            </select>
          </div>
          @error('kategori_id')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Photo</label>
            <input type="file" name="photo" class="form-control">
          </div>
          @error('photo')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Konten</label>
          <textarea name="content" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    @endsection
