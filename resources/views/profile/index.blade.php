@extends('layout.master')
@section('judul')
    Halaman Profile
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/53a1xndfmu73tldyhd86jij8c39zsbmsdyvvpypnxatq25tb/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
   tinymce.init({
      selector: 'textarea',
      plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
    ],
        toolbar: 'undo redo | formatselect | ' +
    'bold italic backcolor | alignleft aligncenter ' +
    'alignright alignjustify | bullist numlist outdent indent | ' +
    'removeformat | help',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        });
  </script>
@endpush
    @section('content')
    <form action="/profile/{{$profile->id}}" method="post">
        @csrf
        @method('PUT')
            <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Nama User</label>
            <input type="text" value="{{$profile->user->name}}" name="name" class="form-control" disabled>
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email User</label>
            <input type="text" value="{{$profile->user->email}}" name="email" class="form-control" disabled>
          </div>

            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Tanggal Lahir</label>
              <input type="date" name="birth_date" class="form-control" value="{{$profile->birth_date}}">
                  <div class="input-group-append">
                    </div>
            </div>
            @error('birth_date')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Gender</label>
                <select class="custom-select" id="gender" name="gender">
                    <option value="Male" @if($profile->gender == 'Male') selected @endif>Male</option>
                    <option value="Female" @if($profile->gender == 'Female') selected @endif>Female</option>
                </select>
            </div>
              @error('gender')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Biodata</label>
            <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>
          </div>
          @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </tbody>
  </table>
  @endsection
